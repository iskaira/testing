const AutoMailing = require('../models/AutoMailing');
const _ccall = require('../..//logger')._ccall;
const Casher = require('../..//Casher');
const Config = require('..//Config');
const _logger = require('../../logger');

const statusHtml = {
    '0': '<span class="status_w label label-danger">Отключен</span>',
    '1': '<span class="status_w label label-success">Активен</span>',
};

module.exports.statusHtml = function (status) {
    return statusHtml[status];
};

/**
 * Создание рассылок
 * @param data
 * @param callback
 */

module.exports.create = function (data, callback) {

    const obj = new AutoMailing(data);

    try {
        obj.save(err => {
            if (err) {
                callback({
                    status: 'error',
                    data: err
                });
                return;
            }
            callback({
                status: 'ok',
                data: obj
            });
        });
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not created',
            line: __fili,
            n_no: 1,
        });
    }
};

/**
 * Обновление рассылки
 * @param id
 * @param updated
 * @param callback
 */
module.exports.update = (id, updated, callback) => {
    updated.updated_at = Date.now();
    try {
        AutoMailing.findOneAndUpdate(
            {_id: id},
            {$set: updated},
            {new: true},
            (err, doc) => {
                if (err) {
                    return callback({
                        status: 'error',
                        data: err
                    });
                }
                callback({
                    status: 'ok',
                    data: doc
                })
            }
        );
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not updated',
            line: __line,
            n_no: 1,
        });
    }
};

/**
 * Обновление 1 рассылки по userId
 * @param id
 * @param updated
 * @param callback
 */
module.exports.updateByUserId = (id, updated, callback) => {
    try {
        AutoMailing.update(
            {userId: id},
            {$set: updated},
            {multi: true},
            (err, doc) => {
                if (err) {
                    return callback({
                        status: 'error',
                        data: err
                    });
                }
                callback({
                    status: 'ok',
                    data: doc
                })
            }
        );
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not updated',
            line: __line,
            n_no: 1,
        });
    }
};

/**
 * Получить кол-во записей
 * @param where
 * @param callback
 */
module.exports.count = function (where, callback) {

    if (typeof where === 'undefined') where = {};

    try {

        // AutoMailing.estimatedDocumentCount(where, function (err, doc) {
        AutoMailing.countDocuments(where, function (err, doc) {
            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить список
 * @param where
 * @param PageData - { skip: 3, limit: 10 }
 * @param callback
 */
module.exports.list = function (where, pageData, callback) {

    if (typeof where === 'undefined') where = {};

    try {

        AutoMailing.find(where, null, pageData, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })

        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить список
 * @param where
 * @param callback
 */

module.exports.getAll = function (where, callback) {
    try {
        AutoMailing.find(where, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить компанию
 * @param id
 * @param callback
 */
module.exports.get = function (id, callback) {
    try {
        AutoMailing.find({
            _id: id
        }, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить рассылки по UserId
 * @param id
 * @param callback
 */
module.exports.getByUserId = function (id, callback) {

    try {
        AutoMailing.find({
            userId: id
        }, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Проверка существования Названии рассылки
 * При условии что поля проиндексированы
 * @param name
 * @param callback
 */
module.exports.checkName = function (seriesId, name, callback) {
    try {

        AutoMailing.find({
                name: name,
                seriesId: seriesId,
                deleted: '0'
            },
            (err, doc) => {
                if (err) {
                    callback({
                        status: 'error',
                        data: err
                    })
                }

                callback({
                    status: 'ok',
                    data: doc,
                })
            })
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'AutoMailing not found',
            line: __fili,
            n_no: 1,
        });
    }
};