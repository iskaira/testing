const statusHtml = {
    '0': '<span class="status_works label label-warning">Запланирован</span>',
    '1': '<span class="status_works label label-success">Отправлен</span>',
    '2': '<span class="status_works label label-danger">Не отправлен</span>',
    '3': '<span class="status_works label label-danger">Не отправлен(Системная)</span>',

};

module.exports.statusHtml = function (status) {
    return statusHtml[status];
};

/**
 * Создание очереди рассылок
 * @param data
 * @param callback
 */

module.exports.create = function (data, callback) {

    const obj = new AutoMailingQueue(data);

    try {
        obj.save(err => {
            if (err) {
                callback({
                    status: 'error',
                    data: err
                });
                return;
            }
            callback({
                status: 'ok',
                data: obj
            });
        });
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not created',
            line: __fili,
            n_no: 1,
        });
    }
};

/**
 * Обновление очереди рассылки
 * @param id
 * @param updated
 * @param callback
 */
module.exports.update = (id, updated, callback) => {
    updated.updated_at = Date.now();
    try {
        AutoMailingQueue.findOneAndUpdate(
            {_id: id},
            {$set: updated},
            {new: true},
            (err, doc) => {
                if (err) {
                    return callback({
                        status: 'error',
                        data: err
                    });
                }
                callback({
                    status: 'ok',
                    data: doc
                })
            }
        );
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not updated',
            line: __line,
            n_no: 1,
        });
    }
};

// delete

/**
 * удаление очереди рассылки
 * @param id
 * @param updated
 * @param callback
 */
module.exports.delete = (where, callback) => {
    // updated.updated_at = Date.now();
    try {
        AutoMailingQueue.deleteMany(
            where,
            (err, doc) => {
                if (err) {
                    return callback({
                        status: 'error',
                        data: err
                    });
                }
                callback({
                    status: 'ok',
                    data: doc
                })
            }
        );
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not updated',
            line: __line,
            n_no: 1,
        });
    }
};

/**
 * Обновление 1 рассылки по userId
 * @param id
 * @param updated
 * @param callback
 */
module.exports.updateByUserId = (id, updated, callback) => {
    try {
        AutoMailingQueue.update(
            {userId: id},
            {$set: updated},
            {multi: true},
            (err, doc) => {
                if (err) {
                    return callback({
                        status: 'error',
                        data: err
                    });
                }
                callback({
                    status: 'ok',
                    data: doc
                })
            }
        );
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not updated',
            line: __line,
            n_no: 1,
        });
    }
};

/**
 * Получить кол-во записей
 * @param where
 * @param callback
 */
module.exports.count = function (where, callback) {

    if (typeof where === 'undefined') where = {};

    try {

        // AutoMailingQueueQueu.estimatedDocumentCount(where, function (err, doc) {
        AutoMailingQueue.countDocuments(where, function (err, doc) {
            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить список
 * @param where
 * @param PageData - { skip: 3, limit: 10 }
 * @param callback
 */
module.exports.list = function (where, pageData, callback) {

    if (typeof where === 'undefined') where = {};

    try {

        AutoMailingQueue.find(where, null, pageData, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })

        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить список
 * @param where
 * @param callback
 */

module.exports.getAll = function (where, callback) {
    try {
        AutoMailingQueue.find(where, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить компанию
 * @param id
 * @param callback
 */
module.exports.get = function (id, callback) {
    try {
        AutoMailingQueue.find({
            _id: id
        }, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить рассылки по UserId
 * @param id
 * @param callback
 */
module.exports.getByUserId = function (id, callback) {

    try {
        AutoMailingQueue.find({
            userId: id
        }, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Массовая вставка рассылок
 * @param req
 * @param res
 * @returns {*}
 */
module.exports.createMany = function(req,res) {
    try {
        AutoMailingQueue.insertMany(req,err => {
            if(err){
                return res({
                    status: 'error',
                    data: err
                });
            }
            return res({
                status: 'ok',
                data: ''
            });
        });
    } catch (e) {
        return res({
            status: 'error',
            data: e
        });
    }
};