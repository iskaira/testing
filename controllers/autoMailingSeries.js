const AutoMailingSeries = require('../models/AutoMailingSeries');
const _ccall = require('../../sendapi-core/logger')._ccall;
const Casher = require('../../sendapi-core/Casher');
const Config = require('../config/Config');
const _logger = require('../../sendapi-core/logger');
const Notyfi = _logger.Notyfi;

const statusHtml = {
    '0': '<span class="status_works label label-danger">Отключен</span>',
    '1': '<span class="status_works label label-success">Активен</span>',
};

module.exports.statusHtml = function (status) {
    return statusHtml[status];
};

/**
 * Создание серии
 * @param data
 * @param callback
 */
module.exports.create = function (data, callback) {

    const obj = new AutoMailingSeries(data);

    try {
        obj.save(err => {
            if (err) {
                callback({
                    status: 'error',
                    data: err
                });
                return;
            }
            callback({
                status: 'ok',
                data: obj
            });
        });
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not created',
            line: __fili,
            n_no: 1,
        });
    }
};

/**
 * Обновление серии
 * @param id
 * @param updated
 * @param callback
 */
module.exports.update = (id, updated, callback) => {
    updated.updated_at = Date.now();
    try {
        AutoMailingSeries.findOneAndUpdate(
            {_id: id},
            {$set: updated},
            {new: true},
            (err, doc) => {
                if (err) {
                    return callback({
                        status: 'error',
                        data: err
                    });
                }
                callback({
                    status: 'ok',
                    data: doc
                })
            }
        );
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not updated',
            line: __line,
            n_no: 1,
        });
    }
};

/**
 * Обновление серии по userId
 * @param id
 * @param updated
 * @param callback
 */
module.exports.updateByUserId = (id, updated, callback) => {
    try {
        AutoMailingSeries.findOneAndUpdate(
            {userId: id},
            {$set: updated},
            {new: true},
            (err, doc) => {
                if (err) {
                    return callback({
                        status: 'error',
                        data: err
                    });
                }
                callback({
                    status: 'ok',
                    data: doc
                })
            }
        );
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'task not updated',
            line: __line,
            n_no: 1,
        });
    }
};

/**
 * Получить кол-во записей
 * @param where
 * @param callback
 */
module.exports.count = function (where, callback) {

    if (typeof where === 'undefined') where = {};

    try {

        // AutoMailingSeries.estimatedDocumentCount(where, function (err, doc) {
        AutoMailingSeries.countDocuments(where, function (err, doc) {
            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить список
 * @param where
 * @param PageData - { skip: 3, limit: 10 }
 * @param callback
 */
module.exports.list = function (where, pageData, callback) {

    if (typeof where === 'undefined') where = {};

    try {

        AutoMailingSeries.find(where, null, pageData, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })

        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить список
 * @param where
 * @param callback
 */

module.exports.getAll = function (where, callback) {
    try {
        AutoMailingSeries.find(where, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить компанию
 * @param id
 * @param callback
 */
module.exports.get = function (id, callback) {
    try {
        AutoMailingSeries.find({
            _id: id
        }, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Получить серии по UserId
 * @param id
 * @param callback
 */
module.exports.getByUserId = function (id, callback) {

    try {
        AutoMailingSeries.find({
            userId: id
        }, function (err, doc) {

            if (err) {
                return callback({
                    status: 'error',
                    data: err
                });
            }

            callback({
                status: 'ok',
                data: doc,
            })
        });

    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'Ошибка',
            _ccall: {
                e: e,
                __fili: __fili,
            },
        });
    }
};

/**
 * Проверка существования Названии серии
 * @param name
 * @param callback
 */
module.exports.checkName = function (name, callback) {
    try {

        AutoMailingSeries.find({
                name: name,
                deleted: '0'
            },
            (err, doc) => {
                if (err) {
                    callback({
                        status: 'error',
                        data: err
                    })
                }

                callback({
                    status: 'ok',
                    data: doc,
                })
            })
    } catch (e) {
        _ccall(callback, {
            status: 'error',
            msg: 'UserRoles not getted',
            line: __fili,
            n_no: 1,
        });
    }
};