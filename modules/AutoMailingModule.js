const _logger = require('../logger');
const logger = _logger.logger;
const rollbar = _logger.rollbar;
const connection = _logger.connection;
const Notyfi = _logger.Notyfi;
const _ccall = _logger._ccall;

const moment = require('moment');
const Twig = require('twig');
const Managers = require('../config/Managers');

const str_replace = require('locutus/php/strings/str_replace');
const empty = require('locutus/php/var/empty');
const isset = require('locutus/php/var/isset');

const Config = require('../config/Config');
const Auth = require('../../sendapi-core/Auth');

const menuAdmin = require('../controllers/menuAdmin');
const autoMailing = require('../controllers/autoMailing');
const autoMailingSeries = require('../controllers/autoMailingSeries');

// const ChannelAmoCRMTasks = require('../controllers/channelAmoCRMTasks');
const Company = require('../controllers/company');

const FileUpload = require('../FileUpload');
const fs = require('fs');
const sizeOf = require('image-size');



/**
 * Для работы с разделом Сериями
 */
AutoMailingModule = {

    page: function (req, res) {

        const self = this;
        Auth.logged_in(req, function (user) {
            if (req.method === 'POST') {
                let body = req.body;

                // Проверяем есть ли файл
                if (typeof req.files === 'undefined' || Object.keys(req.files).length === 0) {
                    req.files = false;
                }

                console.log(body);

                switch (body.action) {
                    case 'save':

                        if (body.seriesId === '' || body.seriesId === undefined) {
                            res.json({status: 'error', msg: 'Не указан ID серии'});
                            return;
                        }

                        if ((body.name === undefined || body.name === '')) {
                            res.json({status: 'error', msg: 'Не указан название рассылки'});
                            return;
                        }

                        if ((body.text === undefined || body.text === '') && !req.files) {
                            res.json({status: 'error', msg: 'Пустые данные для отправки'});
                            return;
                        }

                        if (body.text.length < 10 && !req.files) {
                            res.json({status: 'error', msg: 'Длинна строки должна быть больше 10'});
                            return;
                        }

                        if (body.sendDay === undefined || body.sendDay === '') {
                            res.json({status: 'error', msg: 'День отправки пуст'});
                            return;
                        }

                        if (body.sendTime === undefined || body.sendTime === '') {
                            res.json({status: 'error', msg: 'Время отправки пуст'});
                            return;
                        }

                        if (body.sendTime < '01:00' && body.sendDay <= '1') {
                            res.json({status: 'error', msg: 'Запланнированная рассылка должна быть > 1 дня 01:00 часов'});
                            return;
                        }

                        let data = {
                            companyId: body.companyId,
                            userId: body.userId,
                            seriesId: body.seriesId,

                            delete_file: body.delete_file,

                            name: body.name,
                            text: body.text,
                            sendDay: body.sendDay,
                            sendTime: body.sendTime,
                            status: body.status
                        };

                        if (!!req.files) {

                            const file = req.files.fileUrl[0];
                            console.log(file);
                            const fileTypePattern = /^[^\\?/%*:|"><]+.(.doc|.docx|.pdf|.jpg|.jpeg|.png|.xls|.xlsx|.ppt|.pptx|.txt|.mp3|.wav|.ogg|.opus|.mpeg|.mpg|.avi|.mp4|.wmv)$/
                            if(!file.originalname.match(fileTypePattern))
                            {
                                res.json({status: 'error', msg: 'Неверный тип файла!'});
                                return;
                            }
                            const file_name = str_replace(['\\', '/', ':', '*', '?', '"', '<', '>', '|'], '', file.originalname);

                            const row = {
                                upload_file_path: file.path,
                                file_name: file_name,
                                user_id: 1,
                                ph_numb_from: 'images_mailing',
                            };

                            FileUpload.uploadsFileHandler(row, function (uploadsFileRow) {

                                fs.unlink(file.path, function (err) {
                                });

                                if (uploadsFileRow.status === 'error') {
                                    res.json({
                                        status: 'error',
                                        msg: 'Не удалось загрузить файл'
                                    });
                                    return;
                                }
                                data['file'] = {};
                                data['file']['url'] = '/' + uploadsFileRow.getFileUpload.urlPathName;
                                data['file']['name'] = file_name;
                                data['file']['original_name'] = file.originalname;
                                self.save(data, res, body.id);
                            });
                            return;
                        }
                        self.save(data, res, body.id);
                        break;


                    default:
                        res.json({status: 'error', msg: 'action неверный'});
                }
            }

            if (req.method === 'GET') {

                switch (req.query.action) {

                    // Страница добавления подключения
                    case 'add':
                        if (!req.query.hasOwnProperty('seriesId')) {
                            res.redirect(Config.AUTO_MAILING_URL + '/series');
                            return;
                        }

                        autoMailingSeries.get(req.query.seriesId, function (e) {
                            console.log(e);
                            if (e.status !== 'ok') {
                                res.render('admin/access_denied');
                                return;
                            }
                            if (empty(e.data))
                            {
                                res.redirect(Config.AUTO_MAILING_URL + '/series');
                                return;
                            }

                        res.render('autoMailing/mailing_add_edit', {
                            Config: Config,
                            User: user,
                            seriesData: e.data[0],
                            Menu: menuAdmin.getMenu(req, user.permissions),
                            title: 'Добавить рассылку'
                            });
                        });
                        break;
                    // Страница редактирования Рассылок
                    case 'edit':
                        if (!req.query.hasOwnProperty('id')) {
                            res.redirect(Config.AUTO_MAILING_URL + '/series');
                            return;
                        }
                        const fileTypePhotoPattern = /^[^\\?/%*:|"><]+.(.jpg|.jpeg|.png)$/
                        autoMailing.get(req.query.id, function (e) {
                            console.log(e);
                            if (e.status !== 'ok') {
                                res.redirect(Config.AUTO_MAILING_URL + '/series');
                                return;
                            }

                            if (e.data.length === 0) {
                                res.redirect(Config.AUTO_MAILING_URL + '/series');
                                return;
                            }

                            const mailingData = e.data[0];
                            autoMailingSeries.get(mailingData.seriesId, function (e2) {
                                console.log(e2);
                                if (e2.status !== 'ok') {
                                    res.render(Config.AUTO_MAILING_URL + '/series');
                                    return;
                                }

                                if (e2.data.length === 0) {
                                    res.redirect(Config.AUTO_MAILING_URL + '/series');
                                    return;
                                }

                                if(mailingData.file){
                                    mailingData.file.imageUrl = mailingData.file.url;
                                    if(!mailingData.file.original_name.match(fileTypePhotoPattern))
                                    {
                                        mailingData.file.imageUrl = `${Config.APP_STATIC_URL}/assets/images/file.png`
                                    }
                                }


                                res.render('autoMailing/mailing_add_edit', {
                                    Config: Config,
                                    User: user,
                                    seriesData: e2.data[0],
                                    data: mailingData,
                                    // Menu: menuAdmin.getMenu(req, user.permissions),
                                    title: 'Редактировать рассылку'
                                });

                            });

                        });

                        break;
                    // Получение списка Рассылок
                    case 'list':
                        let mailingWhereData = {deleted:'0'};
                        console.log(req.query);
                        if(req.query.seriesId)
                            mailingWhereData.seriesId = req.query.seriesId;

                        if (req.query.status!==undefined && req.query.status!== 'all') {
                            mailingWhereData.status = req.query.status;
                        }

                        let start = 0;
                        if (req.query.hasOwnProperty('start')) {
                            start = req.query.start;
                        }

                        let length = 0;
                        if (req.query.hasOwnProperty('length')) {
                            length = req.query.length;
                        }
                        console.log(mailingWhereData);
                        autoMailing.count(mailingWhereData, function (count) {
                        if (count.status !== 'ok' || empty(count.data)) {
                            res.json({
                                recordsTotal: 0,
                                recordsFiltered: 0,
                                data: []
                            });
                            return;
                        }
                        console.log(count);
                        let mailing_mass = [];
                        autoMailing.list(mailingWhereData,   {
                            skip: Number(start),
                            limit: Number(length),
                            sort: {expire_date: -1},
                        }, function (e) {

                        if (e.status !== 'ok' || empty(e.data)) {
                            res.json({
                                recordsTotal: 0,
                                recordsFiltered: 0,
                                data: []
                            });
                            return;
                        }
                        // console.log(e);
                        let k = 0;
                        for (let key in e.data) {
                            e.data[key].status = autoMailingSeries.statusHtml(e.data[key].status);
                            e.data[key].lId = k + 1;

                            Twig.renderFile(Config.TEMPLATE_PATH + '/autoMailing/mailing_item.twig', {
                                Config: Config,
                                // companyData: e2.data[0],
                                data: e.data[key]
                            }, function (err, html) {
                                mailing_mass.push({
                                    data: html
                                });
                            k++;
                            if (e.data.length === k) {
                                res.json({
                                    recordsTotal: count.data,
                                    recordsFiltered: count.data,
                                    data: mailing_mass
                                });
                            }
                        });
                        }
                    });
                });
                    break;

                    default:
                        console.log(req.query);
                        res.render('autoMailing/mailing', {
                            Config: Config,
                            User: user,
                            seriesId: req.query.seriesId,
                            // Menu: menuAdmin.getMenu(req, user.permissions),
                            title: 'Список Рассылок'
                        });

                        break;
                }
            }

        }, function (e) {
            res.status(404)
                .render('error', {
                    Config: Config,
                    title: 'ОШИБКА 404'
                });
        });
    },

    save: function (data, res, id) {
        const self = this;
        console.log(data);

        const afterT = moment(data.sendTime, 'HH:mm').add('30', 'minutes').format('HH:mm');
        const beforeT = moment(data.sendTime, 'HH:mm').subtract('30', 'minutes').format('HH:mm');
        let whereData = {
            userId: data.userId,
            sendTime: {
                $gt: beforeT,
                $lt: afterT
            },
            status: '1'
        };
        // При добавлении/сохранении рассылки не учитывать его время
        if (!empty(id))
            whereData._id = {$ne: id};

        console.log(whereData);
        autoMailing.getAll(whereData , function (e) {
            if(e.status!=='ok'){
                console.log(e);
                self.notify('save|autoMailing.getAll', {error: e, data: data});
                return;
            }

            if(!empty(e.data) && e.data.length > 2)
            {
                res.json({
                    status: 'error',
                    msg: `Запланировано больше 2 рассылок в течение получаса [${beforeT} - ${afterT}]`
                });
                return;
            }

            if (typeof id === undefined || id === '') {

                autoMailing.checkName(data.seriesId, data.name, function (check) {
                   if(check.status!=='ok'){
                       res.json({
                           status: 'error',
                           msg: 'Неудалось сохранить',
                           e: check.data
                           });
                   }
                   console.log(check);
                   if(!empty(check.data))
                   {
                       res.json({
                           status: 'error',
                           msg: "Рассылка с таким названием уже существует"
                       });
                       return;
                   }

                    autoMailing.create(data, function (e) {

                        if (e.status !== 'ok') {
                            res.json({
                                status: 'error',
                                msg: 'Неудалось сохранить [' + JSON.stringify(e.data) + ']',
                                e: e.data
                            });
                            return;
                        }

                        res.json({
                            status: 'ok',
                            url: Config.AUTO_MAILING_URL + '/mailing?action=edit&id=' + e.data._id
                            });
                        });
                    });
                return;
            }

            const rootPath   = '/srv/www/sendapi.net/htdocs';
            autoMailing.get(id, function (mailing_result) {
                if(mailing_result.status!=='ok')
                {
                    res.json({
                        status: 'error',
                        msg: 'Неудалось сохранить [' + mailing_result.data + ']',
                        e: mailing_result.data
                    });
                }

                if(data.delete_file==='1'){
                    self.removeFile(rootPath+mailing_result.data[0].file.url);
                    data.file = null;
                }
                if(!empty(data.file) && mailing_result.data[0].file){
                    self.removeFile(rootPath+mailing_result.data[0].file.url);
                }

                autoMailing.update(id, data, function (e) {

                    if (e.status !== 'ok') {
                        res.json({
                            status: 'error',
                            msg: 'Неудалось сохранить [' + e.data.errmsg + ']',
                            e: e.data
                        });
                        return;
                    }

                    res.json({
                        status: 'ok',
                        url: Config.AUTO_MAILING_URL + '/mailing?action=edit&id=' + e.data._id
                    });
                });
            });
        });
    },
    removeFile: function(absoluteFilePath)
    {
        fs.unlink(absoluteFilePath, function(error){
            if(error)
            {
                console.log('remove error', absoluteFilePath);
            }
            console.log("file removed successfully");
        });
    },
    notify: function(method, error) {
        Notyfi.mail_send({
        to: 'kairat.botcorp@gmail.com',
        subject: 'Service-site|AutoMailingModule',
        body: `method ${method} -> error =  ${JSON.stringify(error)}`,
    });
}

};
module.exports = AutoMailingModule;