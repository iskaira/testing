'use strict';
const fs = require('fs');

const moment = require('moment');
const moment_tz = require('moment-timezone');

const empty = require('locutus/php/var/empty');

const Twig = require('twig');
const Config = require('../config/Config');
const menuAdmin = require('../controllers/menuAdmin');
const Auth = require('../Auth');

const Company = require('../controllers/company');
const OrdersController = require("../controllers/orders");
const OrderTasksController = require("../controllers/orderTasks");
const Managers = require('../config/Managers');
const NotifyMe = require('../notify');
const async = require("async");

// const { Worker } = require('worker_threads');

const in_array = require('locutus/php/array/in_array');

const UnixSocketHelper = require('../../sendapi-core/UnixSocketHelper');

// const actionLoggingModule = require('./ActionLoggingModule');

const { StaticPool, DynamicPool } = require("node-worker-threads-pool");
const pool = new DynamicPool(2);
const processOrderWorker = new StaticPool({
    size: 1,
    task: 'statisticsModule.js'
});

const excelJS = require('exceljs');

class Statistics {
    // проверяем файл на дату последней проверки | создания счетов

    constructor() {}

    // Вспомогательные методы
    notify(method, error) {
        NotifyMe.mail_send({
            to: 'kairat.botcorp@gmail.com',
            subject: 'Service-site|StatisticsModule',
            body: `method ${method} -> ${JSON.stringify(error)}`,
        });
    }

    getDashboard(condition, resolve, reject){
        const self = this;
        (async () => {
            try{
                await processOrderWorker.exec({
                    condition: condition,
                    action: 'getDashboard'
                }).then(results => {
                    if (results.status==='ok'){
                        return resolve(results.data);
                    }
                    self.notify('getDashboard|not ok result from worker', {result: results});
                    return reject(results);
                })
                    .catch(handleError => {
                        console.log(handleError);
                        self.notify('getDashboard | processStatisticsWorker | try_catch', {error: handleError});
                        return reject(handleError);
                    });
            } catch (e) {
                console.log('not ok');
                self.notify('getDashboard | try_catch', {error: e});
                reject(e);
            }

        })();
    }

    /**
     * Работа со страницей статистики
     * @param req
     * @param res
     */
    page(req, res) {

        const self = this;

        Auth.permission(req, function (user) {

            if (req.method === 'POST') {

                if (!user.permissionCurrent.sectionEdit) {
                    res.json({status: 'error', msg: 'Нет прав на редактирование раздела.'});
                    return;
                }

                // Проверяем есть ли файл
                if (typeof req.files === 'undefined' || Object.keys(req.files).length === 0) {
                    req.files = false;
                }

                let body = req.body;

                switch (body.action) {

                    case 'download_excel':
                        if (!user.permissionCurrent.sectionDownload) {
                            res.json({status: 'error', msg: 'Нет прав на выгрузку отчета.'});
                            return;
                        }
                        break;
                }
                return;
            }

            if (req.method === 'GET') {

                let condition;

                switch (req.query.action) {
                    case 'getDashboard':
                        if (!user.permissionCurrent.sectionEdit) {
                            res.redirect(Config.ADMIN_URL+'/');
                            return;
                        }

                        condition = {};
                        console.log(self.periodNameToDate(req));
                        if(!empty(self.periodNameToDate(req))){
                            condition.period = self.periodNameToDate(req);
                        }
                        console.log(condition);
                        self.getDashboard(condition,
                            results => {
                                return res.json({
                                    status: 'ok',
                                    data: results
                                });
                            },
                            reject => {
                                console.log(reject);
                                self.notify('getDashboard', reject);
                                return res.json({});
                            });
                        break;

                    c
                        break;

                    default:

                        res.render('dsadasd', {
                            Config: Config,
                            User: user,
                            Menu: menuAdmin.getMenu(req, user.permissions),
                            title: 'asdsadsa'
                        });

                        break;
                }
            }

        }, function (e) {
            console.log(e);
            res.redirect('/');
        });
    }

    /**
     * Мини хелпер
     * Сегодня, Неделя, Месяц, год -> конвертация в ДАТУ(число) для поиска из монги
     * @param req
     * @returns {*}
     */
    periodNameToDate(req) {
        const dateName = req.query.date;
        let start = '';
        let end   = '';
        console.log(req.query);

        if(req.query.customDate){
            start = req.query.customDate.start;
            end = req.query.customDate.end;
        }
        console.log(req.query.start, req.query.end);
        if(req.query.start!=='' && req.query.start!==undefined && req.query.end!==undefined && req.query.end!==''){
            start = req.query.start;
            end = req.query.end;
            return {
                $gte: start,
                $lte: end
            }
        }
        else{

            if(req.query.date ==='' || req.query.date===undefined)
                return {};

            let dateData = {
                'today': {
                    $gte: moment().format('YYYY-MM-DD') + ' 00:00:00',
                    $lte: moment().format('YYYY-MM-DD') + ' 23:59:59'
                },
                'week': {
                    $gte: moment().subtract(7, "days").format('YYYY-MM-DD') + ' 00:00:00',
                    $lte: moment().format('YYYY-MM-DD') + ' 23:59:59'
                },
                'month': {
                    $gte: moment().subtract(1, "month").format('YYYY-MM-DD') + ' 00:00:00',
                    $lte: moment().format('YYYY-MM-DD') + ' 23:59:59'
                },
                'year': {
                    $gte: moment().subtract(1, "year").format('YYYY-MM-DD') + ' 00:00:00',
                    $lte: moment().format('YYYY-MM-DD') + ' 23:59:59'
                },
                'custom': {
                    $gte: start,
                    $lte: end
                }
            };

            return dateData[dateName];
        }
    }


}



const statistics = new Statistics();

module.exports = statistics;
