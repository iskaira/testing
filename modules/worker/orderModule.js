const { workerData, parentPort } = require('worker_threads');

winstonExceptEmail = 'kairat.tilegen@gmail.com';


require('magic-globals');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const fs = require('fs');
const OrderCheckDateFile = "..////OrderCheckDate.json";

const moment = require('moment');
const moment_tz = require('moment-timezone');

const empty = require('locutus/php/var/empty');

const Twig = require('twig');
const Config = require('../../config/Config');

const OrdersController = require("../../controllers/orders");
const OrderTasksController = require("../../controllers/orderTasks");
const Company = require('../../controllers/company'); 

const Auth = require('../../Auth'); // был написан до меня уже
const NotificationModule = require('../NotificationModule');

const NotifyMe = require('../../notify');

const async = require("async");

const in_array = require('locutus/php/array/in_array');


const OrderCheckDateJSON = {
    'createOrder'               : 'lastOrderCreateDate',
    'createOrderForPartners'    : 'lastPartnerOrderCreateDate',
    'expiredOrder'              : 'lastOrderExpireDate',
    'recurringOrder'            : 'lastOrderRecurringDate',
    'firstOrderCreateDate'      : 'lastFirstOrderCreateDate'
};

function processBlockedAccounts(self, resolve, reject){
    const today = moment();
    const today_formatted = today.format('YYYY-MM-DD');

    const updated = {
        status: 'disconnected',
        statusPay: 'disconnected'
    };

    const notification_types = ['email'];

    Company.getAll({
        testEndDate: {
            $lte: today
        }
    },
    company_result => {
            if (company_result.status !== 'ok') {
                notify('processBlockedAccounts->Company', company_result);
                return reject({
                     status: 'error',
                     data: {
                         msg: 'company get error',
                         e: company_result
                     }
                    });
            }
            if (empty(company_result.data)) {
                self.lastBlockDate = today_formatted;
                self.OrderCheckDate.lastBlockDate = today_formatted;
                updateLastCheckFile('processBlockedAccounts', JSON.stringify(self.OrderCheckDate, null,));
                return resolve(self);
            }
            async.filter(company_result.data, function (company, callback) {
                callback(null, 'АААААА')
            },
                function (err, c_filtered_results) {
                    if (err) {
                        notify('processBlockedAccounts async filter', {e: err});
                    }
                    if(empty(c_filtered_results)){
                        return resolve(self);
                    }

                    async.eachSeries(c_filtered_results, function (company_item, callback) {
                        updateCompanyById(company_item._id, updated, 'processBlockedAccounts',
                        data => {
                                const notification_data = {
                                    companyId: data._id,
                                    userId: data.userId,
                                    mail_type: '10' // аккаунт отключен
                                };
                                NotificationModule.createNotificationTasks(notification_types, notification_data);
                        });
                        callback();
                    }, function (err) {
                        // if any of the file processing produced an error, err would equal that error
                        if (err) {
                            console.log(err);
                            notify('processBlockedAccounts async.each err', {e: err});
                        }
                        // blocking bot
                        blockUser();
                        return resolve(self);
                    });
                });
            });
}



// Вспомогательные методы
// Я только вызываю, сокеты я не делал
function blockUser() {
    UnixSocketHelper.sendToServerMyDb(
        '/socket',
        {
            mode: 'blocked_users',
            data: {}
        }, function (resRow) {
            // notify('BlockingUsers->processBlockedAccounts->Company', resRow);
            console.log('BlockingUsers|...', resRow);
        });
    UnixSocketHelper.sendToServerMyDb('/server.socket', {
        mode : 'blocked_users',
        data : {}
    },function (resRow) {
        //console.log('setDeliveryStatusComplete|sendToServerMyDb|resRow', resRow);
    });
}


function notify(method, error) {
    NotifyMe.mail_send({
        to: 'МОЙ@gmail.com',
        subject: 'Сабжеект',
        body: `method ${method} -> ${JSON.stringify(error)}`,
    });
}

/**
 * Проверка на отмену всех счетов
 * @param order_data
 * @param company_data
 * @param resolve
 * @param reject
 */
function checkForUpdateExpiredOrder(order_data, company_data, resolve, reject) {
    if(empty(company_data.orderCreateDate)){
        return resolve()
    }
    if(moment(order_data.expire_date) >= moment(company_data.orderCreateDate)){
        return resolve()
    }
    // В другом случае просто нужно отменить старые счета или не надо
    else
        return reject()
}

parentPort.on("message", (param) => {

    const action = param.action;
    const self = param.self;
    const mode = param.mode;
    // console.log(param);

    switch (action) {
        case 'ФФФФ':
            processOrderExpiredTasks(
                () => {
                    parentPort.postMessage(
                        {
                            status: 'ok',
                            data: []
                        });
                },
                reason => parentPort.postMessage(reason)
            );
            break;
        }

});
