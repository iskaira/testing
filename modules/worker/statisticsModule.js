const { workerData, parentPort } = require('worker_threads');

winstonExceptEmail = 'kairat.tilegen@gmail.com';

require('magic-globals');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const fs = require('fs');

const moment = require('moment');
const moment_tz = require('moment-timezone');

const empty = require('locutus/php/var/empty');

const Twig = require('twig');
const Config = require('../../config/Config');

const menuAdmin = require('../../controllers/menuAdmin');
const OrdersController = require("../../controllers/orders");
const OrderTasksController = require("../../controllers/orderTasks");
const Company = require('../../controllers/company');

const NotificationModule = require('../NotificationModule');

const NotifyMe = require('../../notify');
const async = require("async");

const in_array = require('locutus/php/array/in_array');

console.log('statistics one');
function notify(method, error) {
    NotifyMe.mail_send({
        to: 'kairat.botcorp@gmail.com',
        subject: 'Сервис | modules | worker | название',
        body: `method ${method} -> ${JSON.stringify(error)}`,
    });
}

function getRecurringPaymentCompanies(companyData, resolve, reject) {
    try{
        async.filter(companyData, function (company, callback) {
            callback(null, company.paymentMethods === 'autoPay');
        }, function (err, result) {
            if(err){
                console.log(err);
                notify('getRecurringPaymentCompanies', {error: err, data: companyData});
            }
            // console.log(result);
            resolve(result)
        });
    }
    catch (e) {
        reject({
            status: 'error',
            data: {
                msg: 'getRecurringPaymentCompanies | try.catch error',
                error: e,
                data: companyData
            }
        })
    }
}



function getCustomPaymentCompanies(companyData, resolve, reject) {
    try{
        async.filter(companyData, function (company, callback) {
            callback(null, company.paymentMethods === 'custom');
        }, function (err, result) {
            if(err){
                console.log(err);
                notify('getCustomPaymentCompanies', {error: err, data: companyData});
            }
            // console.log(result);
            resolve(result)
        });
    }
    catch (e) {
        reject({
            status: 'error',
            data: {
                msg: 'getCustomPaymentCompanies | try.catch error',
                error: e,
                data: companyData
            }
        })
    }
}

function getPaymentMethodsCompanies(companyData, resolve, reject){
    try{
        let resultData = {
            custom: {
                KZT: 0,
                RUB: 0,
                USD: 0,
                KZT_count: 0,
                USD_count: 0,
                RUB_count: 0,
            },
            autoPay:  {
                KZT: 0,
                RUB: 0,
                USD: 0,
                KZT_count: 0,
                USD_count: 0,
                RUB_count: 0,
            },
            checkingAccount:  {
                KZT: 0,
                RUB: 0,
                USD: 0,
                KZT_count: 0,
                USD_count: 0,
                RUB_count: 0,
            },
            custom_count: 0,
            autoPay_count: 0,
            checkingAccount_count: 0,
            undefined_count: 0
        };

        async.eachSeries(companyData, function (company, callback) {
            if(company.tariffAmount && company.tariffAmount > 0){
                resultData[`${company.paymentMethods}_count`] += 1;
                resultData[`${company.paymentMethods}`][company.currency] += company.tariffAmount;
                resultData[`${company.paymentMethods}`][`${company.currency}_count`] += 1;
            }
            callback();
        }, function (err) {
            if(err){
                console.log(err);
                notify('getPaymentMethodsCompanies', {error: err, data: companyData});
            }
            resolve(resultData)
        });
    }
    catch (e) {
        reject({
            status: 'error',
            data: {
                msg: 'getPaymentMethodsCompanies | try.catch error',
                error: e,
                data: companyData
            }
        })
    }
}




/**
 *
 * @param companyData - data
 * @param resolve
 * @param reject
 */
function getNotTestModeCompanies(companyData, resolve, reject){
    try{
        async.filter(companyData, function (company, callback) {
            callback(null, company.testMode!=='1');
        }, function (err, result) {
            if(err){
                console.log(err);
                notify('getAddedCompanies', {error: err, data: companyData});
            }
            resolve(result)
        });
    }
    catch (e) {
        reject({
            status: 'error',
            data: {
                msg: 'getAddedCompanies | try.catch error',
                error: e,
                data: companyData
            }
        })
    }
}



/**
 *
 * @param companyData - data
 * @param resolve
 * @param reject
 */
function getTurnedFromTestToPaidCompanies(companyData, resolve, reject){
    try{
        async.filter(companyData, function (company, callback) {
            let compareDate = company.orderCreateDate ? company.orderCreateDate : company.firstOrderCreateDate;
            compareDate = moment(compareDate);
            let testEndDate = company.testEndDate;
            if (testEndDate){
                testEndDate = moment(company.testEndDate).add(15, 'days'); // 1 month не подходит, потому что при создании клиента вообще не правильно делали
                callback(null, testEndDate && testEndDate.isSameOrBefore(compareDate));
            }
            else{
                testEndDate = company.status==='work';
                callback(null, testEndDate);
            }
        }, function (err, result) {
            if(err){
                console.log(err);
                notify('getAddedCompanies', {error: err, data: companyData});
            }
            resolve(result)
        });
    }
    catch (e) {
        reject({
            status: 'error',
            data: {
                msg: 'getAddedCompanies | try.catch error',
                error: e,
                data: companyData
            }
        })
    }


function getDirectCompanies(companyData, resolve, reject){
    try{
        async.filter(companyData, function (company, callback) {
            callback(null, company.checkPartner!==true);
        }, function (err, result) {
            if(err){
                console.log(err);
                notify('getDirectCompanies', {error: err, data: companyData});
            }
            resolve(result)
        });
    }
    catch (e) {
        reject({
            status: 'error',
            data: {
                msg: 'getDirectCompanies | try.catch error',
                error: e,
                data: companyData
            }
        })
    }
}



function getCompanyDataByPeriodAndData(condition, companyData, resolve, reject) {
    try{
        let conditionData = {
            '$gte': '2010-01-01 00:00:00',
            '$lte': '2030-12-31 23:59:59',
        };
        let filter_condition = 1;
        if(!empty(condition) && condition.period) {
            conditionData = condition.period;
            filter_condition = 1;
        }
        if(!empty(condition) && condition['$gte']) {
            filter_condition = 0;
            conditionData['$gte'] = condition['$gte'];
        }

        async.filter(companyData, function (company, callback) {
            if (filter_condition===1)
                callback(null, moment(company.addDate).isBetween(moment(conditionData['$gte']), moment(conditionData['$lte'])));
            else
                callback(null, moment(company.addDate).isSameOrBefore(moment(conditionData['$gte'])));

        }, function (err, result) {
            if(err){
                console.log(err);
                notify('getDirectCompanies', {error: err, data: companyData});
            }
            resolve(result)
        });
    }
    catch (e) {
        reject({
            status: 'error',
            data: {
                msg: 'getDirectCompanies | try.catch error',
                error: e,
                data: companyData
            }
        })
    }
}


/**
 * Цифры для визуализации в статистике
 * @param condition - должен быть только по проиндексированным полям
 * @param resolve
 * @param reject
 */
function processDashboardData(condition, resolve, reject){
    let company_condition = {};
    let company_condition_till_gte = {};
    if(condition && condition.period){
         company_condition.addDate = condition.period;
         company_condition_till_gte['$gte'] = condition.period['$gte'];
    }
    console.log(condition);


    Company.getAll({}, company_result =>{
        if(company_result.status==='ok'){
            getCompanyDataByPeriodAndData(company_condition_till_gte, company_result.data,
            getCompanyDataByPeriodAndDataResALL => { // До $gte
                getProductionCompanies(getCompanyDataByPeriodAndDataResALL,
                    getProductionCompaniesResALL => { // До $gte
                        getActiveCompanies(condition, getProductionCompaniesResALL, // До $gte
                        getActiveCompaniesResALL => {
                            calculateAmount(getActiveCompaniesResALL, // До $gte
                            calculateAmountResALL => {
                                resultsData.clients_revenue_all = calculateAmountResALL; // До $gte
                                getCompanyDataByPeriodAndData(condition, company_result.data,
                                    getCompanyDataByPeriodAndDataRes => {
                                                    // Тут провааааааааааааааааааааааааааааааааааааааааааааааааааал
                                                    // до заполнения resultsData
                                },  
                                reason => {return reject(reason)});
                            },
                            reason => {return reject(reason)});
                        },
                        reason => {return reject(reason)})
                    },
                reason => {return reject(reason)})
                },
            reason => {return reject(reason)})

        }
    });
}



parentPort.on("message", (param) => {

    const action = param.action;
    const condition = param.condition;
    const report_type = param.report_type;
    switch (action) {
        case 'getDashboard':
            processDashboardData(condition,
                results => {
                    console.log('finished:', param.action);
                    parentPort.postMessage(
                        {
                            status: 'ok',
                            data: results
                        });
                },
                reason => {
                console.log(reason);
                parentPort.postMessage({
                    status: 'error'
                });
            });
            break;
    }
});
