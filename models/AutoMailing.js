const mongoose = require('mongoose');
const mongooseConnect = mongoose.createConnection('mongodb://localhost:27017/sendapi_site', {
    useNewUrlParser: true,
    useCreateIndex: true,
});
const Schema = mongoose.Schema;

// Схема списка сообщении
const schema = new Schema({
    transport: {
        type    : String,
        enum    : ['whatsapp','vk','instagram','telegram','viber','facebook','yandex'],
        default : 'whatsapp'
    },
    companyId: {
        type    : String,
        index   : true,
    },
    userId: {
        type    : Number,
        index   : true,
    },

    seriesId: {
        type    : String,
        index   : true,
    },

    status: {
        type    : String,
        enum    : ['0', '1'],
        default : '1'
    },

    deleted: {
        type    : String,
        enum    : ['0', '1'],
        default : '0'
    },

    name: {
        type    : String,
    },
    text: {
        type    : String
    },
    // url, name, original name
    file: {
        type: {}
    },
    fileUrl: {
        type    : String
    },
    fileName: {
        type    : String
    },
    tags: {
      type      : []
    },

    sendDay: {
        type: Number
    },
    sendTime: {
        type: String
    },

    created_at: {
        type    : Date,
        default : new Date(),
        index   : true,
    },
    updated_at: {
        type    : Date,
        default : new Date()
    }
});

module.exports = mongooseConnect.model('auto_mailing', schema);