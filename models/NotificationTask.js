const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const connection = mongoose.createConnection('connection', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
});

// NotificationTasks Model
const notificationTasksSchema = Schema({
    // Тип задачи
    mode: {
        type: String,
        enum: ['sms', 'email', 'whatsapp'],
        required: true,
    },
    // Передаваемый объект data:
    // mail_type - 0,1,2,3, custom
    // message_data - default:undefined, if custom -> message_data: { text, file}
    data: {
        type: {},
        required: true,
    },
    // Статус задачи
    status:{
        // Статус задачи (
        // 0 - создана в ожидании,
        // 1 - в работе,
        // 2 - выполнена
        // 3 - ошибка в ходе выполнения)
        type: String,
        enum: ['0','1','2','3'],
        index: true,
        required: true
    },
    send_time: {
        type: Date,
        // required:   true,
    },

    // Или же системные делать. Тогда нужно убрать
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date
    }
});
module.exports = connection.model('notification_task', notificationTasksSchema);
