const mongoose = require('mongoose');
const mongooseConnect = mongoose.createConnection('mongodb://localhost:27017/sendapi_site', {
    useNewUrlParser: true,
    useCreateIndex: true,
});
const Schema = mongoose.Schema;

// Схема списка сообщении
const schema = new Schema({
    companyId: {
        type    : String,
        index   : true,
    },
    userId: {
        type    : Number,
        index   : true,
    },

    seriesId: {
        type    : String,
        index   : true,
    },

    mailingId: {
        type    : String,
        index   : true,
    },
    // 0 - не отправлен, 1 - отправлен, 2 - ошибка, 3 - отмена
    send_status: {
        type    : String,
        enum    : ['0', '1', '2'],
        default : '0'
    },

    deleted: {
        type    : String,
        enum    : ['0', '1'],
        default : '0'
    },

    sendDateTime: {
        type    : Date,
        index   : true
    },
    // Filter
    userRegisteredDate: {
        type    : Date,
        index   : true
    },
    // сгенерирован системно(автоматом)
    auto: {
        type    : String,
        enum    : ['0', '1'],
        default : '0'
    },

    mailing_id: {
        type    : String
    },
    timezone: { // name and +00:00
        type: {}
    },

    error_code: {
        type    : String,
    },
    error_description: {
        type    : String,

    },

    created_at: {
        type    : Date,
        default : new Date()
    },
    updated_at: {
        type    : Date,
        default : new Date()
    }
});

module.exports = mongooseConnect.model('auto_mailing_queue', schema);