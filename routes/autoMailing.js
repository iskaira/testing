const _logger = require('../../sendapi-core/logger');
const logger = _logger.logger;
const rollbar = _logger.rollbar;
const connection = _logger.connection;
const multer = require('multer');
const upload = multer({dest: '/srv/www/sendapi.net/nodejs/service-site/uploads/'});

const express = require('express');
const router = express.Router();
const Config = require('../config/Config');

// const PaymentConfig = require('../config/PaymentConfig');
const Auth = require('../../sendapi-core/Auth');
const Twig = require('twig');

const AutoMailingSeriesModule = require('../modules/AutoMailingSeriesModule');
const AutoMailingModule = require('../modules/AutoMailingModule');
const AutoMailingQueueModule = require('../modules/AutoMailingQueueModule');
//
const NotificationModule = require("../modules/NotificationModule");

const CompaniesController = require('../controllers/company');
const OrdersController = require('../controllers/orders');
const OrderTasksController = require("../controllers/orderTasks");
const TransactionsController = require("../controllers/transactions");
const UserNotificationController = require("../controllers/userNotifications");

const UnixSocketHelper = require('../../sendapi-core/UnixSocketHelper');
const Helpers = require('../Helpers');

const moment = require('moment');

const NotifyMe = require('../notify');

/*
autoMailing Transaction
 */

// MailingSeries - Серии
router.get('/series', function(req, res) {AutoMailingSeriesModule.page(req, res);});
router.post('/series', function(req, res) {AutoMailingSeriesModule.page(req, res);});

// MailingQueue - Список очереди рассылок
router.get('/mailing', function(req, res) {AutoMailingModule.page(req, res);});
router.post('/mailing', upload.fields([{name: 'fileUrl', maxCount: 1}]), function(req, res) {AutoMailingModule.page(req, res);});


// MailingQueue - Список очереди рассылок
router.get('/mailing_queue', function(req, res) {AutoMailingQueueModule.page(req, res);});
router.post('/mailing_queue', function(req, res) {AutoMailingQueueModule.page(req, res);});


module.exports = router;
